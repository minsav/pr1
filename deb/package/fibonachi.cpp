

#include <iostream>
#include <stdlib.h>
#include <string>
#include <sstream>
using namespace std;

bool isPositiveInteger(const std::string& str) {
    if (str.empty()) {
        return false;
    }

    std::istringstream iss(str);
    int number;
    if (!(iss >> number)) {
        return false;
    }
    return number > 0 && iss.eof();
}


int main()
{
    //bool exit = true;
    
    //do{

        int number = 0;//номер числа, которое надо найти
        string input;
        int num1 = 0;
        int num2 = 1;
        int sum = 0;
        bool flag = true;
        setlocale(LC_ALL, "Russian");
        cout << endl;
        cout << "Введите номер числа в последовательности Фибоначчи:\n";
        //if (_getch() == 27) {
            //break;
        //}



        do {

            std::getline(std::cin, input);
            if (isPositiveInteger(input)) {
                flag = false;
                number = stoi(input);
            }
            else {
                cout << "Введите положительное целое число!\n";
                cin.clear();
                fflush(stdin);
            }
        } while (flag);

        if (number == 1) {
            cout << "1 число в последовательности Фибоначчи - 0\n";
        }
        else if (number == 2) {
            cout << "2 число в последовательности Фибоначчи - 1\n";
        }
        else {
            for (int i = 2; i < number; i++) {
                sum = num1 + num2;
                num1 = num2;
                num2 = sum;
            }
            cout << number << " число в последовательности Фибоначчи - " << sum << endl << endl;
        }
        

    //} while (exit);

    return 0;
   


}

